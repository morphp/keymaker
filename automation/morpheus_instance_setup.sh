#!/bin/bash


accessToken="your api access token here"


branch="main"
serverUrl="<%= morpheus.applianceUrl %>"
taskID=$(curl -XPOST "$serverUrl/api/tasks" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"task": {
    "name": "Keymaker Morpheus Install",
    "code": "installStatus",
    "resultType": "keyValue",
    "taskType": {
      "code": "script"
    },
    "taskOptions": {
      "shell.sudo": "on"
      },
    "executeTarget": "resource",
    "file": {
      "sourceType": "url",
      "contentPath":"https://gitlab.com/morphp/keymaker/-/raw/'$branch'/automation/instance_type_scripts/install_morph.sh?inline=false"
    }
  }}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["task"]["id"])')

getMorpLicTaskID=$(curl -XPOST "$serverUrl/api/tasks" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"task": {
    "name": "Keymaker Morpheus Get License",
    "code": "myLicenseKey",
    "resultType": "value",
    "taskType": {
      "code": "script"
    },
    "executeTarget": "local",
    "file": {
      "sourceType": "url",
      "contentPath":"https://gitlab.com/morphp/keymaker/-/raw/'$branch'/automation/instance_type_scripts/keyMakerGetMorpheusLic.sh?inline=false"
    }
  }}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["task"]["id"])')


  keymakerAddCloudCredsTaskID=$(curl -XPOST "$serverUrl/api/tasks" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"task": {
    "name": "Keymaker Add Cloud Creds to Cypher",
    "taskType": {
      "code": "jythonTask"
    },
    "taskOptions": {
      "pythonAdditionalPackages": "requests cryptography"
      },
    "executeTarget": "local",
    "file": {
      "sourceType": "url",
      "contentPath":"https://gitlab.com/morphp/keymaker/-/raw/'$branch'/automation/instance_type_scripts/keyMakerAddCreds.py?inline=false"
      
    }
  }}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["task"]["id"])')
  

  KeymakerAddCloudsTaskID=$(curl -XPOST "$serverUrl/api/tasks" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"task": {
    "name": "Keymaker Add Clouds To Appliance",
    "taskType": {
      "code": "jythonTask"
    },
    "taskOptions": {
      "pythonAdditionalPackages": "requests cryptography"
      },
    "executeTarget": "local",
    "file": {
      "sourceType": "url",
      "contentPath":"https://gitlab.com/morphp/keymaker/-/raw/'$branch'/automation/instance_type_scripts/keyMakerAddClouds.py?inline=false"
      
    }
  }}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["task"]["id"])')


  uninstallTaskID=$(curl -XPOST "$serverUrl/api/tasks" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"task": {
    "name": "kmUninstall",
    "taskType": {
      "code": "jythonTask"
    },
    "taskOptions": {
      "pythonAdditionalPackages": "requests"
      },
    "executeTarget": "local",
    "file": {
      "sourceType": "url",
      "contentPath":"https://gitlab.com/morphp/keymaker/-/raw/'$branch'/automation/instance_type_scripts/kmUninstall.py?inline=false"
      
    }
  }}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["task"]["id"])')





  
workflowID=$(curl -XPOST "$serverUrl/api/task-sets" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"taskSet": {
    "name": "Keymaker Morpheus Install PWF",
    "tasks": [
      {
        "taskId": '$getMorpLicTaskID',
        "taskPhase": "postProvision"
      },
      {
        "taskId": '$taskID',
        "taskPhase": "postProvision"
      },
      {
        "taskId": '$KeymakerAddCloudsTaskID',
        "taskPhase": "postProvision"
      }
    ]
  }}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["taskSet"]["id"])')
  


branchOptionListID=$(curl -XPOST "$serverUrl/api/library/option-type-lists" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{
    "optionTypeList": {
        "name": "keyMakerBranchList",
        "description": null,
        "type": "manual",
        "sourceMethod": "GET",
        "apiType": null,
        "ignoreSSLErrors": true,
        "realTime": true,
        "visibility": "private",
        "initialDataset": "[{\"name\":\"Standard\",\"value\":\"standard\"},{\"name\":\"LTS\",\"value\":\"lts\"},{\"name\":\"All\",\"value\":\"all\"}]",
        "config": {}
    }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionTypeList"]["id"])')

branchSelectInputID=$(curl -XPOST "$serverUrl/api/library/option-types" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"optionType": {
       "name": "Keymaker Branch Input",
       "type": "select",
       "description": "Keymaker Branch Input ",
       "fieldName": "keyMakerBranchInput",
       "code":"keyMakerBranchInput",
       "fieldLabel": "Branch",
       "required": true,
       "optionList": {
            "id": '$branchOptionListID'
        }
     }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionType"]["id"])')



optionListID=$(curl -XPOST "$serverUrl/api/library/option-type-lists" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{
    "optionTypeList": {
        "name": "keyMakerMorpheusVersionList",
        "description": null,
        "type": "rest",
        "sourceUrl": "http://wa-morpheus-v-01.uksouth.cloudapp.azure.com:8080/morpheus-versions/?aKey=ynpXeL$yyctuCUYp9m$Fc5sgPqcV",
        "sourceMethod": "GET",
        "apiType": null,
        "ignoreSSLErrors": true,
        "realTime": true,
        "visibility": "private",
        "requestScript": "results.push({name:\"branch\", value : input.keyMakerBranchInput})",
        "config": {}
    }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionTypeList"]["id"])')

selectInputID=$(curl -XPOST "$serverUrl/api/library/option-types" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"optionType": {
       "name": "Keymaker Morpheus Version",
       "type": "select",
       "code":"keyMakerMorpheusVersion",
       "description": "Keymaker Morpheus Version List ",
       "fieldName": "keyMakerMorpheusVersion",
       "fieldLabel": "Morpheus Version",
       "dependsOnCode": "keyMakerBranchInput",
       "optionList": {
            "id": '$optionListID'
        },
        "required": true
     }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionType"]["id"])')

cypherOptionListID=$(curl -XPOST "$serverUrl/api/library/option-type-lists" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{
    "optionTypeList": {
        "name": "keyMakerMorpCypherOpList",
        "description": null,
        "type": "rest",
        "sourceUrl": "'$serverUrl'/api/cypher",
        "sourceMethod": "GET",
        "apiType": null,
        "ignoreSSLErrors": true,
        "realTime": true,
        "visibility": "private",
        "config": {
            "sourceHeaders": [
                {
                    "name": "Authorization",
                    "value": "Bearer '$accessToken'",
                    "masked": false
                }
            ]
        },
        "translationScript": "for(var x=0;x < data.cyphers.length; x++) {\r\n  //Only load keymaker cypher mount\r\n  cypherKeymakerMount = data.cyphers[x].itemKey.split(\"/\")[1];\r\n cypherKeymakerType = data.cyphers[x].itemKey.split(\"/\")[2];\r\n  if (cypherKeymakerMount == \"keymaker\" && cypherKeymakerType == \"license\" ){\r\n  \tresults.push({name:data.cyphers[x].itemKey,value:data.cyphers[x].itemKey});\r\n  }\r\n  }"
    }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionTypeList"]["id"])')



usernameInputID=$(curl -XPOST "$serverUrl/api/library/option-types" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"optionType": {
       "name": "Keymaker Morpheus Username",
       "type": "text",
       "description": "Keymaker Morpheus Username Text Input ",
       "fieldName": "keyMakerMorpUsernameInput",
       "fieldLabel": "Admin Username",
       "required": true
     }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionType"]["id"])')

passwordInputID=$(curl -XPOST "$serverUrl/api/library/option-types" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"optionType": {
       "name": "Keymaker Morpheus Password",
       "type": "password",
       "description": "Keymaker Morpheus Password Text Input ",
       "fieldName": "keyMakerMorpPasswordInput",
       "fieldLabel": "Admin Password",
       "required": true
     }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionType"]["id"])')

cypherSelectInputID=$(curl -XPOST "$serverUrl/api/library/option-types" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"optionType": {
       "name": "Keymaker Morpheus Cypher",
       "type": "select",
       "description": "Keymaker Morpheus Cypher Select Input ",
       "fieldName": "keyMakerMorpCypherInput",
       "fieldLabel": "Morpheus License",
       "optionList": {
            "id": '$cypherOptionListID',
            "name": "keyMakerMorpCypherOpList"
        },
     }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionType"]["id"])')


#Clouds Automation
#Create Option Lists

keyMakerAddCloudsOpListID=$(curl -XPOST "$serverUrl/api/library/option-type-lists" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{
    "optionTypeList": {
        "name": "keyMakerAddCloudsOpList",
        "description": null,
        "type": "rest",
        "sourceUrl": "'$serverUrl'/api/cypher",
        "sourceMethod": "GET",
        "apiType": null,
        "ignoreSSLErrors": true,
        "realTime": true,
        "visibility": "private",
        "config": {
            "sourceHeaders": [
                {
                    "name": "Authorization",
                    "value": "Bearer '$accessToken'",
                    "masked": false
                }
            ]
        },
        "translationScript": "for(var x=0;x < data.cyphers.length; x++) {\r\n  //Only load keymaker cypher mount\r\n  cypherKeymakerMount = data.cyphers[x].itemKey.split(\"/\")[1];\r\n  cypherKeymakerType = data.cyphers[x].itemKey.split(\"/\")[2];\r\n  cypherKeymakerResourceType = data.cyphers[x].itemKey.split(\"/\")[3];\r\n  cypherKeymakerCloud = data.cyphers[x].itemKey.split(\"/\")[4] + \"/\" + data.cyphers[x].itemKey.split(\"/\")[5];\r\n  if (cypherKeymakerMount == \"keymaker\" && cypherKeymakerType == \"credentials\" && cypherKeymakerResourceType == \"cloud\" ){\r\n  \tresults.push({name:cypherKeymakerCloud,value:data.cyphers[x].itemKey});\r\n  }\r\n  }"
    }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionTypeList"]["id"])')


keyMakerAWSRegionsOpListID=$(curl -XPOST "$serverUrl/api/library/option-type-lists" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{
    "optionTypeList": {
        "name": "keyMakerAWSRegionsOpList",
        "description": null,
        "type": "manual",
        "sourceMethod": "GET",
        "apiType": null,
        "ignoreSSLErrors": true,
        "realTime": true,
        "visibility": "private",
        "initialDataset": "[\r\n {\r\n   \"name\": \"US East (Ohio)\",\r\n   \"value\": \"ec2.us-east-2.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"US East (N. Virginia)\",\r\n   \"value\": \"ec2.us-east-1.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"US West (N. California)\",\r\n   \"value\": \"ec2.us-west-1.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"US West (Oregon)\",\r\n   \"value\": \"ec2.us-west-2.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"Africa (Cape Town)\",\r\n   \"value\": \"ec2.af-south-1.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"Asia Pacific (Hong Kong)\",\r\n   \"value\": \"ec2.ap-east-1.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"Asia Pacific (Hyderabad)\",\r\n   \"value\": \"ec2.ap-south-2.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"Asia Pacific (Jakarta)\",\r\n   \"value\": \"ec2.ap-southeast-3.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"Asia Pacific (Mumbai)\",\r\n   \"value\": \"ec2.ap-south-1.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"Asia Pacific (Osaka)\",\r\n   \"value\": \"ec2.ap-northeast-3.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"Asia Pacific (Seoul)\",\r\n   \"value\": \"ec2.ap-northeast-2.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"Asia Pacific (Singapore)\",\r\n   \"value\": \"ec2.ap-southeast-1.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"Asia Pacific (Sydney)\",\r\n   \"value\": \"ec2.ap-southeast-2.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"Asia Pacific (Tokyo)\",\r\n   \"value\": \"ec2.ap-northeast-1.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"Canada (Central)\",\r\n   \"value\": \"ec2.ca-central-1.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"Europe (Frankfurt)\",\r\n   \"value\": \"ec2.eu-central-1.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"Europe (Ireland)\",\r\n   \"value\": \"ec2.eu-west-1.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"Europe (London)\",\r\n   \"value\": \"ec2.eu-west-2.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"Europe (Milan)\",\r\n   \"value\": \"ec2.eu-south-1.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"Europe (Paris)\",\r\n   \"value\": \"ec2.eu-west-3.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"Europe (Spain)\",\r\n   \"value\": \"ec2.eu-south-2.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"Europe (Stockholm)\",\r\n   \"value\": \"ec2.eu-north-1.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"Europe (Zurich)\",\r\n   \"value\": \"ec2.eu-central-2.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"Middle East (Bahrain)\",\r\n   \"value\": \"ec2.me-south-1.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"Middle East (UAE)\",\r\n   \"value\": \"ec2.me-central-1.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"South America (S‹o Paulo)\",\r\n   \"value\": \"ec2.sa-east-1.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"AWS GovCloud (US-East)\",\r\n   \"value\": \"ec2.us-gov-east-1.amazonaws.com\"\r\n },\r\n {\r\n   \"name\": \"AWS GovCloud (US-West)\",\r\n   \"value\": \"ec2.us-gov-west-1.amazonaws.com\"\r\n }\r\n]",
        "config": {}
    }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionTypeList"]["id"])')


keyMakerCloudTypeOpListID=$(curl -XPOST "$serverUrl/api/library/option-type-lists" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{
    "optionTypeList": {
        "name": "keyMakerCloudTypeOpList",
        "description": null,
        "type": "manual",
        "sourceMethod": "GET",
        "apiType": null,
        "ignoreSSLErrors": true,
        "realTime": true,
        "visibility": "private",
        "initialDataset": "[{\"name\":\"Azure\",\"value\":\"azure\"},{\"name\":\"Amazon\",\"value\":\"amazon\"},{\"name\":\"Google\",\"value\":\"google\"},{\"name\":\"VMware\",\"value\":\"vmware\"}]",
        "config": {}
    }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionTypeList"]["id"])')

keyMakerGcpRegionsOpListID=$(curl -XPOST "$serverUrl/api/library/option-type-lists" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{
    "optionTypeList": {
        "name": "keyMakerGcpRegionsOpList",
        "description": null,
        "type": "manual",
        "sourceMethod": "GET",
        "apiType": null,
        "ignoreSSLErrors": true,
        "realTime": true,
        "visibility": "private",
        "initialDataset": "[\r\n {\r\n   \"name\": \"All\",\r\n   \"value\": \"all\"\r\n },\r\n {\r\n   \"name\": \"asia-east1\",\r\n   \"value\": \"asia-east1\"\r\n },\r\n {\r\n   \"name\": \"asia-east2\",\r\n   \"value\": \"asia-east2\"\r\n },\r\n {\r\n   \"name\": \"asia-northeast1\",\r\n   \"value\": \"asia-northeast1\"\r\n },\r\n {\r\n   \"name\": \"asia-northeast2\",\r\n   \"value\": \"asia-northeast2\"\r\n },\r\n {\r\n   \"name\": \"asia-northeast3\",\r\n   \"value\": \"asia-northeast3\"\r\n },\r\n {\r\n   \"name\": \"asia-south1\",\r\n   \"value\": \"asia-south1\"\r\n },\r\n {\r\n   \"name\": \"asia-south2\",\r\n   \"value\": \"asia-south2\"\r\n },\r\n {\r\n   \"name\": \"asia-southeast1\",\r\n   \"value\": \"asia-southeast1\"\r\n },\r\n {\r\n   \"name\": \"asia-southeast2\",\r\n   \"value\": \"asia-southeast2\"\r\n },\r\n {\r\n   \"name\": \"australia-southeast1\",\r\n   \"value\": \"australia-southeast1\"\r\n },\r\n {\r\n   \"name\": \"australia-southeast2\",\r\n   \"value\": \"australia-southeast2\"\r\n },\r\n {\r\n   \"name\": \"europe-central2\",\r\n   \"value\": \"europe-central2\"\r\n },\r\n {\r\n   \"name\": \"europe-north1\",\r\n   \"value\": \"europe-north1\"\r\n },\r\n {\r\n   \"name\": \"europe-southwest1\",\r\n   \"value\": \"europe-southwest1\"\r\n },\r\n {\r\n   \"name\": \"europe-west1\",\r\n   \"value\": \"europe-west1\"\r\n },\r\n {\r\n   \"name\": \"europe-west2\",\r\n   \"value\": \"europe-west2\"\r\n },\r\n {\r\n   \"name\": \"europe-west3\",\r\n   \"value\": \"europe-west3\"\r\n },\r\n {\r\n   \"name\": \"europe-west4\",\r\n   \"value\": \"europe-west4\"\r\n },\r\n {\r\n   \"name\": \"europe-west6\",\r\n   \"value\": \"europe-west6\"\r\n },\r\n {\r\n   \"name\": \"europe-west8\",\r\n   \"value\": \"europe-west8\"\r\n },\r\n {\r\n   \"name\": \"europe-west9\",\r\n   \"value\": \"europe-west9\"\r\n },\r\n {\r\n   \"name\": \"me-west1\",\r\n   \"value\": \"me-west1\"\r\n },\r\n {\r\n   \"name\": \"northamerica-northeast1\",\r\n   \"value\": \"northamerica-northeast1\"\r\n },\r\n {\r\n   \"name\": \"northamerica-northeast2\",\r\n   \"value\": \"northamerica-northeast2\"\r\n },\r\n {\r\n   \"name\": \"southamerica-east1\",\r\n   \"value\": \"southamerica-east1\"\r\n },\r\n {\r\n   \"name\": \"southamerica-west1\",\r\n   \"value\": \"southamerica-west1\"\r\n },\r\n {\r\n   \"name\": \"us-central1\",\r\n   \"value\": \"us-central1\"\r\n },\r\n {\r\n   \"name\": \"us-east1\",\r\n   \"value\": \"us-east1\"\r\n },\r\n {\r\n   \"name\": \"us-east4\",\r\n   \"value\": \"us-east4\"\r\n },\r\n {\r\n   \"name\": \"us-east5\",\r\n   \"value\": \"us-east5\"\r\n },\r\n {\r\n   \"name\": \"us-south1\",\r\n   \"value\": \"us-south1\"\r\n },\r\n {\r\n   \"name\": \"us-west1\",\r\n   \"value\": \"us-west1\"\r\n },\r\n {\r\n   \"name\": \"us-west2\",\r\n   \"value\": \"us-west2\"\r\n },\r\n {\r\n   \"name\": \"us-west3\",\r\n   \"value\": \"us-west3\"\r\n },\r\n {\r\n   \"name\": \"us-west4\",\r\n   \"value\": \"us-west4\"\r\n }\r\n]",
        "config": {}
    }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionTypeList"]["id"])')


keyMakerAddCloudsInputID=$(curl -XPOST "$serverUrl/api/library/option-types" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"optionType": {
       "name": "Keymaker Morpheus Add Clouds",
       "type": "typeahead",
       "description": "Keymaker Morpheus Add Clouds Typeahead Input ",
       "fieldName": "keyMakerAddCloudsInput",
       "fieldLabel": "Add Clouds",
       "required": true,
       "optionList": {
            "id": '$keyMakerAddCloudsOpListID',
            "name": "keyMakerAddCloudsOpList"
        },
      "config": {
            "multiSelect": "on"
        },
     }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionType"]["id"])')


keymakerAwsAccessKeyInputID=$(curl -XPOST "$serverUrl/api/library/option-types" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"optionType": {
       "name": "Keymaker AWS Access Key Input",
       "type": "text",
       "description": "Keymaker Morpheus AWS Access Key Input ",
       "fieldName": "keymakerAwsAccessKeyInput",
       "fieldLabel": "Access Key",
       "required": false,
       "visibleOnCode": "keymakerCloudTypeInput:(amazon)",
        "requireOnCode": "keymakerCloudTypeInput:(amazon)"
     }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionType"]["id"])')

keymakerAwsRegionsSelectInputID=$(curl -XPOST "$serverUrl/api/library/option-types" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"optionType": {
       "name": "Keymaker AWS Regions Input",
       "type": "select",
       "description": "Keymaker Morpheus AWS Region Select Input ",
       "fieldName": "keymakerAwsRegionsInput",
       "fieldLabel": "Region",
       "optionList": {
            "id": '$keyMakerAWSRegionsOpListID',
            "name": "keyMakerAWSRegionsOpList"
        },
      "visibleOnCode": "keymakerCloudTypeInput:(amazon)",
      "requireOnCode": "keymakerCloudTypeInput:(amazon)"
     }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionType"]["id"])')


keymakerAwsSecretKeyInputID=$(curl -XPOST "$serverUrl/api/library/option-types" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"optionType": {
       "name": "Keymaker AWS Secret Key Input",
       "type": "text",
       "description": "Keymaker Morpheus AWS Secret Key Input ",
       "fieldName": "keymakerAwsSecretKeyInput",
       "fieldLabel": "Secret Key",
       "required": false,
       "visibleOnCode": "keymakerCloudTypeInput:(amazon)",
       "requireOnCode": "keymakerCloudTypeInput:(amazon)"
     }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionType"]["id"])')

keymakerAzureClientInputID=$(curl -XPOST "$serverUrl/api/library/option-types" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"optionType": {
       "name": "Keymaker Azure Client ID Input",
       "type": "text",
       "description": "Keymaker Morpheus Azure Client Input ",
       "fieldName": "keymakerAzureClientIDInput",
       "fieldLabel": "Azure Client ID",
       "required": false,
       "visibleOnCode": "keymakerCloudTypeInput:(azure)",
       "requireOnCode": "keymakerCloudTypeInput:(azure)"
     }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionType"]["id"])')

keymakerAzureClientSecretInputID=$(curl -XPOST "$serverUrl/api/library/option-types" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"optionType": {
       "name": "Keymaker Azure Client Secret Input",
       "type": "text",
       "description": "Keymaker Morpheus Azure Client Secret Input ",
       "fieldName": "keymakerAzureClientSecretInput",
       "fieldLabel": "Azure Client Secret",
       "required": false,
       "visibleOnCode": "keymakerCloudTypeInput:(azure)",
       "requireOnCode": "keymakerCloudTypeInput:(azure)"
     }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionType"]["id"])')

keymakerAzureSubIDInputID=$(curl -XPOST "$serverUrl/api/library/option-types" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"optionType": {
       "name": "Keymaker Azure Sub ID Input",
       "type": "text",
       "description": "Keymaker Morpheus Azure Subscription ID Input ",
       "fieldName": "keymakerAzureSubIDInput",
       "fieldLabel": "Azure Subscription ID",
       "required": false,
       "visibleOnCode": "keymakerCloudTypeInput:(azure)",
       "requireOnCode": "keymakerCloudTypeInput:(azure)"
     }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionType"]["id"])')

keymakerAzureTenantInputID=$(curl -XPOST "$serverUrl/api/library/option-types" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"optionType": {
       "name": "Keymaker Azure Tenant ID Input",
       "type": "text",
       "description": "Keymaker Morpheus Azure Tenant ID Input ",
       "fieldName": "keymakerAzureTenantIDInput",
       "fieldLabel": "Azure Tenant ID",
       "required": false,
       "visibleOnCode": "keymakerCloudTypeInput:(azure)",
       "requireOnCode": "keymakerCloudTypeInput:(azure)"
     }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionType"]["id"])')

keymakerCloudCredNameInputID=$(curl -XPOST "$serverUrl/api/library/option-types" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"optionType": {
       "name": "Keymaker Cloud Credential Name Input",
       "type": "text",
       "description": "Keymaker Cloud Credential Name Input ",
       "fieldName": "keymakerCloudCredNameInput",
       "fieldLabel": "Credential Name",
       "required": true
     }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionType"]["id"])')


keymakerCloudTypeInputID=$(curl -XPOST "$serverUrl/api/library/option-types" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"optionType": {
       "name": "Keymaker Cloud Type Input",
       "type": "select",
       "description": "Keymaker Cloud Type Input",
       "fieldName": "keymakerCloudTypeInput",
       "fieldLabel": "Cloud Type",
       "required": true,
       "optionList": {
            "id": '$keyMakerCloudTypeOpListID',
            "name": "keyMakerCloudTypeOpList"
        },
     }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionType"]["id"])')


keymakerGcpClientEmailInputID=$(curl -XPOST "$serverUrl/api/library/option-types" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"optionType": {
       "name": "Keymaker GCP Client Email Input",
       "type": "text",
       "description": "Keymaker GCP Client Email Input",
       "fieldName": "keymakerGcpClientEmailInput",
       "fieldLabel": "CLIENT EMAIL",
       "required": false,
       "visibleOnCode": "keymakerCloudTypeInput:(google)",
       "requireOnCode": "keymakerCloudTypeInput:(google)"

     }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionType"]["id"])')


keymakerGcpPrivateKeyInputID=$(curl -XPOST "$serverUrl/api/library/option-types" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"optionType": {
       "name": "Keymaker GCP Private Key Input",
       "type": "text",
       "description": "Keymaker GCP Private Key Input",
       "fieldName": "keymakerGcpPrivateKeyInput",
       "fieldLabel": "PRIVATE KEY",
       "required": false,
       "visibleOnCode": "keymakerCloudTypeInput:(google)",
       "requireOnCode": "keymakerCloudTypeInput:(google)"

     }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionType"]["id"])')


keymakerGcpProjectIdInputID=$(curl -XPOST "$serverUrl/api/library/option-types" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"optionType": {
       "name": "Keymaker GCP Project ID Input",
       "type": "text",
       "description": "Keymaker GCP Project ID Input",
       "fieldName": "keymakerGcpProjectIdInput",
       "fieldLabel": "PROJECT ID",
       "required": false,
       "visibleOnCode": "keymakerCloudTypeInput:(google)",
       "requireOnCode": "keymakerCloudTypeInput:(google)"

     }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionType"]["id"])')

keymakerGcpRegionInputID=$(curl -XPOST "$serverUrl/api/library/option-types" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"optionType": {
       "name": "Keymaker GCP Region Input",
       "type": "select",
       "description": "Keymaker GCP Region Input",
       "fieldName": "keymakerGcpRegionInput",
       "fieldLabel": "REGION",
       "optionList": {
            "id": '$keyMakerGcpRegionsOpListID',
            "name": "keyMakerGcpRegionsOpList"
        },
       "visibleOnCode": "keymakerCloudTypeInput:(google)",
       "requireOnCode": "keymakerCloudTypeInput:(google)"
     }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionType"]["id"])')


keymakerVMwareApiUrlInputID=$(curl -XPOST "$serverUrl/api/library/option-types" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"optionType": {
       "name": "Keymaker VMware Api Url Input",
       "type": "text",
       "description": "Keymaker VMware Api Url Input",
       "fieldName": "keymakerVMwareApiUrlInput",
       "fieldLabel": "Api URL",
       "required": false,
       "visibleOnCode": "keymakerCloudTypeInput:(vmware)",
       "requireOnCode": "keymakerCloudTypeInput:(vmware)"

     }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionType"]["id"])')


keymakerVmwareCredsPasswordInputID=$(curl -XPOST "$serverUrl/api/library/option-types" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"optionType": {
       "name": "Keymaker VMware Creds Password",
       "type": "password",
       "description": "Keymaker VMware Creds Password",
       "fieldName": "keymakerVmwareCredsPassword",
       "fieldLabel": "Password",
       "required": false,
       "visibleOnCode": "keymakerCloudTypeInput:(vmware)",
       "requireOnCode": "keymakerCloudTypeInput:(vmware)"

     }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionType"]["id"])')


keymakerVmwareCredsUsernameInputID=$(curl -XPOST "$serverUrl/api/library/option-types" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{"optionType": {
       "name": "Keymaker VMware Creds Username",
       "type": "text",
       "description": "Keymaker VMware Creds Username",
       "fieldName": "keymakerVmwareCredsUsername",
       "fieldLabel": "Username",
       "required": false,
       "visibleOnCode": "keymakerCloudTypeInput:(vmware)",
       "requireOnCode": "keymakerCloudTypeInput:(vmware)"

     }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["optionType"]["id"])')



keymakerAddCloudCredsOWFID=$(curl -XPOST "$serverUrl/api/task-sets" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{
     "taskSet": {
          "type": "operation",
          "visibility": "private",
          "name": "Keymaker Add Cloud Credentials",
          "tasks":[
                    {
                      "taskPhase": "operation",
                      "taskId": '$keymakerAddCloudCredsTaskID'
                    }
                  ],
          "optionTypes":['$keymakerCloudCredNameInputID',
                         '$keymakerCloudTypeInputID',
                         '$keymakerAzureSubIDInputID',
                         '$keymakerAzureTenantInputID',
                         '$keymakerAzureClientInputID',
                         '$keymakerAzureClientSecretInputID',
                         '$keymakerVmwareCredsUsernameInputID',
                         '$keymakerVmwareCredsPasswordInputID',
                         '$keymakerVMwareApiUrlInputID',
                         '$keymakerAwsRegionsSelectInputID',
                         '$keymakerAwsAccessKeyInputID',
                         '$keymakerAwsSecretKeyInputID',
                         '$keymakerGcpPrivateKeyInputID',
                         '$keymakerGcpClientEmailInputID',
                         '$keymakerGcpProjectIdInputID',
                         '$keymakerGcpRegionInputID']
     }
}' | python3 -c 'import sys, json; print(json.load(sys.stdin)["taskSet"]["id"])')



#Create instance-------------------------------

imageID=$(curl "$serverUrl/api/virtual-images/?name=Morpheus%20Ubuntu%2022.04%20v1&filterType=System" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
 | python3 -c 'import sys, json; print(json.load(sys.stdin)["virtualImages"][0]["id"])')
 
if [ -z "$imageID" ]; then
  echo "\$imageID is empty"
else
  echo "Image ID = $imageID"
  nodeID=$(curl -XPOST "$serverUrl/api/library/container-types" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  -d '{
  "containerType": {
    "name": "KeyMaker Node VMware",
    "shortName": "keyMakerNodeVMware",
    "containerVersion": "1.0",
    "provisionTypeCode": "vmware",
    "virtualImageId":'$imageID',
    "serverType":"vm"
    }
  }' | python3 -c 'import sys, json; print(json.load(sys.stdin)["containerType"]["id"])')
  echo "node = $nodeID"
  if [ -z "$nodeID" ]; then
    echo "nodeID is empty"
  else

    instanceTypeID=$(curl -XPOST "$serverUrl/api/library/instance-types" \
      -H "Authorization: BEARER $accessToken" \
      -H "Content-Type: application/json" \
      -k \
      -d '{
    "instanceType": {
        "name": "Keymaker Morpheus App",
        "code": "keyMakerMorpheusApp",
        "description": "Morpheus Applaince Instance Type",
        "category": "apps"
    }
    }' | python3 -c 'import sys, json; print(json.load(sys.stdin)["instanceType"]["id"])')
      if [ -z "$instanceTypeID" ]; then
        echo "instanceTypeID is empty"
      else
        echo "InstanceTypeID = $instanceTypeID"
        layoutTypeID=$(curl -XPOST "$serverUrl/api/library/instance-types/$instanceTypeID/layouts" \
          -H "Authorization: BEARER $accessToken" \
          -H "Content-Type: application/json" \
          -k \
          -d '{
          "instanceTypeLayout": {
            "name": "keyMaker Morpheus VMware",
            "instanceVersion": "1.0",
            "description": "Keymaker Morpheus VMware Layout",
            "provisionTypeCode": "vmware",
            "memoryRequirement": "8000",
            "containerTypes": [
              '$nodeID'
            ],
            "optionTypes": [
            '$branchSelectInputID',
            '$selectInputID',
            '$cypherSelectInputID',
            '$usernameInputID',
            '$passwordInputID',
            '$keyMakerAddCloudsInputID'
            ],
            "taskSetId":'$workflowID'
        }
        }' | python3 -c 'import sys, json; print(json.load(sys.stdin)["instanceTypeLayout"]["id"])')
        if [ -z "$layoutTypeID" ]; then
          echo "layoutTypeID is empty"
        else
          echo "layoutTypeID = $layoutTypeID"
        fi
      fi
  fi
fi


#Azure Layout

# Get Azure Ubuntu node id
AzureNodeID=$(curl "$serverUrl/api/library/container-types?provisionType=Azure&code=ubuntu-azure-22.04" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  | python3 -c 'import sys, json; print(json.load(sys.stdin)["containerTypes"][0]["id"])')
  echo "Ubuntu Azure Node ID = $AzureNodeID"
  if [ -z "$AzureNodeID" ]; then
    echo "Ubuntu Azure Node ID Not Found"
  else

  #Create Azure Ubuntu Layout
  azureLayoutTypeID=$(curl -XPOST "$serverUrl/api/library/instance-types/$instanceTypeID/layouts" \
          -H "Authorization: BEARER $accessToken" \
          -H "Content-Type: application/json" \
          -k \
          -d '{
          "instanceTypeLayout": {
            "name": "KeyMaker Morpheus Azure",
            "instanceVersion": "1.0",
            "description": "Keymaker Morpheus Azure Layout",
            "provisionTypeCode": "azure",
            "memoryRequirement": "8000",
            "containerTypes": [
              '$AzureNodeID'
            ],
            "optionTypes": [
            '$branchSelectInputID',
            '$selectInputID',
            '$cypherSelectInputID',
            '$usernameInputID',
            '$passwordInputID',
            '$keyMakerAddCloudsInputID'
            ],
            "taskSetId":'$workflowID'
        }
        }' | python3 -c 'import sys, json; print(json.load(sys.stdin)["instanceTypeLayout"]["id"])')
        if [ -z "$azureLayoutTypeID" ]; then
          echo "azureLayoutTypeID is empty"
        else
          echo "azureLayoutTypeID = $azureLayoutTypeID"
        fi
  
  fi


#Amazon Layout

# Get Amazon Ubuntu node id
AmazonNodeID=$(curl "$serverUrl/api/library/container-types?provisionType=amazon&code=ubuntu-amazon-22.04" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  | python3 -c 'import sys, json; print(json.load(sys.stdin)["containerTypes"][0]["id"])')
  echo "Ubuntu Amazon Node ID = $AmazonNodeID"
  if [ -z "$AmazonNodeID" ]; then
    echo "Ubuntu Amazon Node ID Not Found"
  else

  #Create Amazon Ubuntu Layout
  amazonLayoutTypeID=$(curl -XPOST "$serverUrl/api/library/instance-types/$instanceTypeID/layouts" \
          -H "Authorization: BEARER $accessToken" \
          -H "Content-Type: application/json" \
          -k \
          -d '{
          "instanceTypeLayout": {
            "name": "KeyMaker Morpheus Amazon",
            "instanceVersion": "1.0",
            "description": "Keymaker Morpheus Amazon Layout",
            "provisionTypeCode": "amazon",
            "memoryRequirement": "8000",
            "containerTypes": [
              '$AmazonNodeID'
            ],
            "optionTypes": [
            '$branchSelectInputID',
            '$selectInputID',
            '$cypherSelectInputID',
            '$usernameInputID',
            '$passwordInputID',
            '$keyMakerAddCloudsInputID'
            ],
            "taskSetId":'$workflowID'
        }
        }' | python3 -c 'import sys, json; print(json.load(sys.stdin)["instanceTypeLayout"]["id"])')
        if [ -z "$amazonLayoutTypeID" ]; then
          echo "amazonLayoutTypeID is empty"
        else
          echo "amazonLayoutTypeID = $azureLayoutTypeID"
        fi
  
  fi


#Google Layout

# Get Google Ubuntu node id
GoogleNodeID=$(curl "$serverUrl/api/library/container-types?provisionType=google&code=google.layout.public.template.22.04m.ubuntu" \
  -H "Authorization: BEARER $accessToken" \
  -H "Content-Type: application/json" \
  -k \
  | python3 -c 'import sys, json; d =json.load(sys.stdin); print(d["containerTypes"][0]["id"] if len(d["containerTypes"]) > 0 else print (d) )')
  
  if [ "$GoogleNodeID" -gt 0 ]; then
    echo "Ubuntu Google Node ID = $GoogleNodeID"
     #Create Google Ubuntu Layout
        googleLayoutTypeID=$(curl -XPOST "$serverUrl/api/library/instance-types/$instanceTypeID/layouts" \
                -H "Authorization: BEARER $accessToken" \
                -H "Content-Type: application/json" \
                -k \
                -d '{
                "instanceTypeLayout": {
                    "name": "KeyMaker Morpheus Google",
                    "instanceVersion": "1.0",
                    "description": "Keymaker Morpheus Google Layout",
                    "provisionTypeCode": "google",
                    "memoryRequirement": "8000",
                    "containerTypes": [
                    '$GoogleNodeID'
                    ],
                    "optionTypes": [
                    '$branchSelectInputID',
                    '$selectInputID',
                    '$cypherSelectInputID',
                    '$usernameInputID',
                    '$passwordInputID',
                    '$keyMakerAddCloudsInputID'
                    ],
                    "taskSetId":'$workflowID'
                }
                }' | python3 -c 'import sys, json; print(json.load(sys.stdin)["instanceTypeLayout"]["id"])')
                if [ -z "$googleLayoutTypeID" ]; then
                echo "googleLayoutTypeID is empty"
                else
                echo "googleLayoutTypeID = $googleLayoutTypeID"
                fi
  else
    echo "Error getting ubuntu Google Node Type for keymaker. This could be due to not having GCP cloud integrated on your current appliance - $GoogleNodeID"
  fi




echo "Username Input Option ID = $usernameInputID"
echo "Password Input Option ID = $passwordInputID"
echo "Cypher Option List ID = $cypherOptionListID"
echo "Cypher Select Input ID = $cypherSelectInputID"
echo "branchSelectInputID = $branchSelectInputID"
echo "branchOptionListID = $branchOptionListID"
echo "Option List ID( Morpheus version) = $optionListID"
echo "Select Input ID (Morpheus Versioin) = $selectInputID"
echo "Install task id = $taskID"
echo "Get Keymaker License Task ID = $getMorpLicTaskID"
echo "Workflow id = $workflowID"
echo "Uninstall Task id = $uninstallTaskID"

echo "keymakerAddCloudCredsTaskID = $keymakerAddCloudCredsTaskID"
echo "KeymakerAddCloudsTaskID = $KeymakerAddCloudsTaskID"
echo "keyMakerAddCloudsOpListID = $keyMakerAddCloudsOpListID"
echo "keyMakerAWSRegionsOpListID = $keyMakerAWSRegionsOpListID"
echo "keyMakerCloudTypeOpListID = $keyMakerCloudTypeOpListID"
echo "keyMakerGcpRegionsOpListID = $keyMakerGcpRegionsOpListID"
echo "keyMakerAddCloudsInputID = $keyMakerAddCloudsInputID"
echo "keymakerAwsAccessKeyInputID = $keymakerAwsAccessKeyInputID"
echo "keymakerAwsRegionsSelectInputID = $keymakerAwsRegionsSelectInputID"
echo "keymakerAwsSecretKeyInputID = $keymakerAwsSecretKeyInputID"
echo "keymakerAzureClientInputID = $keymakerAzureClientInputID"
echo "keymakerAzureClientSecretInputID = $keymakerAzureClientSecretInputID"
echo "keymakerAzureSubIDInputID = $keymakerAzureSubIDInputID"
echo "keymakerAzureTenantInputID = $keymakerAzureTenantInputID"
echo "keymakerCloudCredNameInputID = $keymakerCloudCredNameInputID"
echo "keymakerCloudTypeInputID = $keymakerCloudTypeInputID"
echo "keymakerGcpClientEmailInputID = $keymakerGcpClientEmailInputID"
echo "keymakerGcpPrivateKeyInputID = $keymakerGcpPrivateKeyInputID"
echo "keymakerGcpProjectIdInputID = $keymakerGcpProjectIdInputID"
echo "keymakerGcpRegionInputID = $keymakerGcpRegionInputID"
echo "keymakerVMwareApiUrlInputID = $keymakerVMwareApiUrlInputID"
echo "keymakerVmwareCredsPasswordInputID = $keymakerVmwareCredsPasswordInputID"
echo "keymakerVmwareCredsUsernameInputID = $keymakerVmwareCredsUsernameInputID"
echo "keymakerAddCloudCredsOWFID = $keymakerAddCloudCredsOWFID"






