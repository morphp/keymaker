import json
import requests


serverUrl=morpheus['morpheus']['applianceUrl']
accessToken=morpheus['morpheus']['apiAccessToken']


resources = [{'resource_type':'instanceTypes','resource_endpoint':'library/instance-types'},
             {'resource_type':'containerTypes','resource_endpoint':'library/container-types'},
             {'resource_type':'taskSets','resource_endpoint':'task-sets'},
             {'resource_type':'tasks','resource_endpoint':'tasks'},
             {'resource_type':'optionTypes','resource_endpoint':'library/option-types'},
             {'resource_type':'optionTypeLists','resource_endpoint':'library/option-type-lists'}
]

def getFromMorpheus(app_srv,api_token,endpoint):

    url = app_srv+endpoint
    response = requests.get(url,
    headers = {
        "Content-Type": "text/plain",
        "Accept": "*/*",
        "Authorization": "Bearer "+accessToken,
        "Accept-Encoding": None
        },
    verify = False)
    return json.loads(response.text)


def deleteFromMorpheus(app_srv,api_token,endpoint):
    url = app_srv+endpoint
    print('delete url ='+str(url))
    response = requests.delete(url,
    headers = {
        "Content-Type": "text/plain",
        "Accept": "*/*",
        "Authorization": "Bearer "+accessToken,
        "Accept-Encoding": None
        },
    verify = False)
    return json.loads(response.text)

#check if there are any keymaker type provisioned instances
api_endpoint = "api/instances?instanceType=keyMakerMorpheusApp" 
response = getFromMorpheus(serverUrl,accessToken,api_endpoint)
instances = response['instances']
total_number_of_instances = len(instances)
instanace_ids = ''
for i in instances:#instances to remove
    instanace_ids = instanace_ids + " "+str(i['id'])+" "

#Only delete resources if no instances are found
if total_number_of_instances > 0 :
	print(str(total_number_of_instances) +' keyMaker instances detected. Remove instances ('+instanace_ids+') before removing the keyMaker instance type and its resources')
else: 
    print("Deleting keyMaker Instance type and resources")
    for r in resources:
        api_endpoint = "api/"+r['resource_endpoint']
        foundResources = getFromMorpheus(serverUrl,accessToken,api_endpoint+"?phrase=keymaker")[r['resource_type']]
        for resource in foundResources:
            rsp = deleteFromMorpheus(serverUrl,accessToken,api_endpoint+"/"+str(resource['id']))
            print('Delete = id='+str(resource['id'])+ " name= "+resource['name']+ " type= "+r['resource_type']+ " Delete Status = "+str(rsp))
            