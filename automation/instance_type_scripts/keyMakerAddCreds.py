import json
import requests
from cryptography.fernet import Fernet

def inputExists(morpheus,inputName):
    if inputName in morpheus['customOptions']:
        return True

serverUrl=morpheus['morpheus']['applianceUrl']
accessToken=morpheus['morpheus']['apiAccessToken']

cloudType=morpheus['customOptions']['keymakerCloudTypeInput']
cloudCredName=morpheus['customOptions']['keymakerCloudCredNameInput']

#Azure Inputs
subscription_id=str(morpheus['customOptions']['keymakerAzureSubIDInput']) if inputExists(morpheus,'keymakerAzureSubIDInput') else 'N/A'
tenant_id=str(morpheus['customOptions']['keymakerAzureTenantIDInput']) if inputExists(morpheus,'keymakerAzureTenantIDInput') else 'N/A'
client_id=str(morpheus['customOptions']['keymakerAzureClientIDInput']) if inputExists(morpheus,'keymakerAzureClientIDInput') else 'N/A'
client_secret=str(morpheus['customOptions']['keymakerAzureClientSecretInput']) if inputExists(morpheus,'keymakerAzureClientSecretInput') else 'N/A'

#Vmware Inputs
vmware_api_url=str(morpheus['customOptions']['keymakerVMwareApiUrlInput']) if inputExists(morpheus,'keymakerVMwareApiUrlInput') else 'N/A'
vmware_username=str(morpheus['customOptions']['keymakerVmwareCredsUsername']) if inputExists(morpheus,'keymakerVmwareCredsUsername') else 'N/A'
vmware_password=str(morpheus['customOptions']['keymakerVmwareCredsPassword']) if inputExists(morpheus,'keymakerVmwareCredsPassword') else 'N/A'

#Amazon Inputs
amazon_region=str(morpheus['customOptions']['keymakerAwsRegionsInput']) if inputExists(morpheus,'keymakerAwsRegionsInput') else 'N/A'
amazon_access_key=str(morpheus['customOptions']['keymakerAwsAccessKeyInput']) if inputExists(morpheus,'keymakerAwsAccessKeyInput') else 'N/A'
amazon_secret_key=str(morpheus['customOptions']['keymakerAwsSecretKeyInput']) if inputExists(morpheus,'keymakerAwsSecretKeyInput') else 'N/A'

#GCP Inputs
google_client_email=str(morpheus['customOptions']['keymakerGcpClientEmailInput']) if inputExists(morpheus,'keymakerGcpClientEmailInput') else 'N/A'
google_private_key=str(morpheus['customOptions']['keymakerGcpPrivateKeyInput']) if inputExists(morpheus,'keymakerGcpPrivateKeyInput') else 'N/A'
google_project_id=str(morpheus['customOptions']['keymakerGcpProjectIdInput']) if inputExists(morpheus,'keymakerGcpProjectIdInput') else 'N/A'
google_region=str(morpheus['customOptions']['keymakerGcpRegionInput']) if inputExists(morpheus,'keymakerGcpRegionInput') else 'N/A'

d={}


#Get Keychain


keyChain=''
encoding='utf-8'
response = requests.get(serverUrl+"/api/cypher/secret/keymaker/keychain",
headers = {
    "Content-Type": "text/plain",
    "Accept": "*/*",
    "Authorization": "Bearer "+accessToken,
    "Accept-Encoding": None
    },
verify = False)

if response.status_code==404:
    
    key = Fernet.generate_key()
    d={'key':str(key,encoding)}
    myData=json.dumps(d).encode('utf-8')
    response = requests.post(serverUrl+"/api/cypher/secret/keymaker/keychain",
    headers = {
        "Content-Type": "text/plain",
        "Accept": "*/*",
        "Authorization": "Bearer "+accessToken,
        "Accept-Encoding": None
        },
    data = myData,
    verify = False)
    keyChain=key
else:
    
    keyChain=response.json()['data']['key'].encode(encoding)



fernet = Fernet(keyChain)


if cloudType=='azure':
    d={
        'subscription_id':str(fernet.encrypt(subscription_id.encode()),encoding),
        'tenant_id':str(fernet.encrypt(tenant_id.encode()),encoding),
        'client_id':str(fernet.encrypt(client_id.encode()),encoding),
        'client_secret':str(fernet.encrypt(client_secret.encode()),encoding)
    }
elif cloudType=='vmware':
     d={
        'vmware_api_url':str(fernet.encrypt(vmware_api_url.encode()),encoding),
        'vmware_username':str(fernet.encrypt(vmware_username.encode()),encoding),
        'vmware_password':str(fernet.encrypt(vmware_password.encode()),encoding)
    }
elif cloudType=='amazon':
     d={
        'amazon_region':str(fernet.encrypt(amazon_region.encode()),encoding),
        'amazon_access_key':str(fernet.encrypt(amazon_access_key.encode()),encoding),
        'amazon_secret_key':str(fernet.encrypt(amazon_secret_key.encode()),encoding)
    }
elif cloudType=='google':
     d={
        'google_client_email':str(fernet.encrypt(google_client_email.encode()),encoding),
        'google_private_key':str(fernet.encrypt(google_private_key.encode()),encoding),
        'google_project_id':str(fernet.encrypt(google_project_id.encode()),encoding),
        'google_region':str(fernet.encrypt(google_region.encode()),encoding) 
    }




myData=json.dumps(d).encode('utf-8')
response = requests.post(serverUrl+"/api/cypher/secret/keymaker/credentials/cloud/"+cloudType+"/"+cloudCredName,
headers = {
    "Content-Type": "text/plain",
    "Accept": "*/*",
    "Authorization": "Bearer "+accessToken,
    "Accept-Encoding": None
    },
data = myData,
verify = False)
print(response.json())

