# Keymaker



## Getting started

1. Integrate VMware cloud on your existing appliance (at least 5.4.9-3)

2. Generate an api access token from user settings and make note of it.

3. Add your Morpheus license to the cypher to autmatically apply it to Morpheus instance during provisioning. The cypher key needs to be in the following format (secret/keymaker/name_your_license).

4. To setup the Keymaker instance type, create a local shell script task on your existing appliance with content from the following script, and insert your api access token from step 2, and set the execute target to local:

    'https://gitlab.com/morphp/keymaker/-/blob/main/automation/morpheus_instance_setup.sh'. 

5. Run the task.

After a successful execution it will create the Keymaker instance type automatically.

## Compatibility

Cloud - VMware vCenter

Morpheus Version - 5.4.9-3